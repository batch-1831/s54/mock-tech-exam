let collection = [];

// Write the queue functions below.
// Note: Avoid using Array methods (except .length) on creating the queue functions.

function print() {
	return collection;
}

function enqueue(text) {
	collection[collection.length] = text;
	return collection;
}

function dequeue() {

	// Using for loop:
	for(i = 1; i < collection.length; i++) {
		collection[i - 1] = collection[i];
	};
	collection.length--;
	
	return collection;
	
	// Using Sets
	// // Convert to Set
	// let newSet = new Set(collection);

	// // Get the value of first index
	// const val = newSet.values();
	// let firstIndexValue = val.next().value;
	
	// // Delete first index and convert back to array
	// newSet.delete(firstIndexValue);
	// let newArray = [...newSet];

	// // update collection array
	// collection = newArray;

	// return collection;
}

function front() {
	// easy method
	return collection[0];

	// other method
	// let newSet = new Set(collection);
	// let val = newSet.values()
	// return val.next().value;
}

function size() {
	return collection.length;
}

function isEmpty() {
	if(collection.length == 0) {
		return true;
	}
	else {
		return false;
	}
}

module.exports = {
	//export created queue functions
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};